# aws-cloudfront-multi-origin

AWS Cloudfront + AWS Lambda + AWS Route53 + AWS Certificate Manager


## Architecture

![architecture](architecture.png)


## Reference

* [How to use Amazon CloudFront as an Application Router](https://www.linkedin.com/pulse/how-use-amazon-cloudfront-application-router-chris-iona/)
* [How to route to multiple origins with CloudFront](https://advancedweb.hu/how-to-route-to-multiple-origins-with-cloudfront/)
* [Multi-Origin CloudFront Setup to Route Requests to Services Based on Request Path](https://serebrov.github.io/html/2019-06-16-multi-origin-cloudfront-setup.html)
* [Implementing Default Directory Indexes in Amazon S3-backed Amazon CloudFront Origins Using Lambda@Edge](https://aws.amazon.com/ru/blogs/compute/implementing-default-directory-indexes-in-amazon-s3-backed-amazon-cloudfront-origins-using-lambdaedge/)
* [How to Connect AWS Lightsail to Cloudfront](https://marccreighton.co.uk/amazon-web-services/how-to-connect-aws-lightsail-to-cloudfront/)
* [Hosting Multiple Angular Apps with Path Based Routing in AWS](https://medium.com/@uslperera/hosting-multiple-angular-apps-with-path-based-routing-in-aws-aabd92a83fce)
* [PROTECTING AN AWS ALB BEHIND AN AWS CLOUDFRONT DISTRIBUTION](https://www.arhs-group.com/protecting-aws-alb-behind-aws-cloudfront-distribution/)
